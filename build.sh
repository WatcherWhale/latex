#!/bin/bash

BASE="$PWD"
BUILT="$PWD/built"

rm -rf $BUILT

function recursive_for_loop {
    for f in *;  do
        if [ -d "$f"  -a ! -h "$f" ] && [ "$f" != "built" ];
        then
            cd -- "$f";
            texfiles="$(ls | grep "\.tex")"

            if [ -n "$texfiles" ]; then

                builtdir="$BUILT$(echo $PWD | awk -F"$BASE" '{print $2}')"
                mkdir -p "$builtdir"

                for t in $texfiles; do
                    latexmk -f -pdf -interaction=nonstopmode -silent "$t"
                    cp "${t%".tex"}.pdf" "$builtdir"
                done;
            fi

            recursive_for_loop;
            cd ..;
        fi;
    done;
};
recursive_for_loop
