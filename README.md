# Latex files

[![](https://gitlab.com/WatcherWhale/latex/badges/master/pipeline.svg)](https://gitlab.com/WatcherWhale/latex/-/pipelines)

In this project you can find all my publicaly available latex files.

The latest built files can be found here: [https://gitlab.com/WatcherWhale/latex/-/jobs/artifacts/master/browse?job=Latex](https://gitlab.com/WatcherWhale/latex/-/jobs/artifacts/master/browse?job=Latex)
