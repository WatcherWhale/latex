\documentclass[a4paper,openany]{uantwerpenassignment}

\usepackage[english]{babel}
\usepackage{titlesec}
\usepackage{graphicx}
\usepackage{pythonhighlight}

\definecolor{code}{HTML}{ecf0f1}
\definecolor{codetext}{HTML}{d9534f}
\newcommand{\codeword}[1]{
    \colorbox{code}{\texttt{\textcolor{codetext}{#1}}}
}

\facultyacronym{TI}

\title{Report: Lab 1}
\subtitle{5-Artificial Intelligence}
\author{Mathias Maes\\Tijs Van Alphen}

\programme{BA}{IW}{EI}

\academicyear{2020-2021}

\publisher{}

\titleformat{\chapter}{\normalfont\huge\bfseries}{\thechapter.}{10pt}{\huge\bfseries}

\begin{document}

\maketitle

\tableofcontents
\chapter{Question 1 - 4: Search Algorithms}

\section{General Tree Search}

For question 1 to 4 we used the same basic lines of codes, the only difference is the type of data structure. The type of data structure is the parameter that changes the way the search algorithm behaves. We start by pushing the start state into our data structure. Afterwards we create an array called visited. This array will contain every location we \codeword{visited} so far. 

The search algorithm really begins when we enter the while loop with condition that our data structure isn't empty. We get a location and action by popping the data structure (in the first cycle this will be the start location). We continue by checking if our current node has already been visited (node is present in \codeword{visited}). If that is the case then we'll pop the next location/action. When this is not the case the meaning the node is not yet visited, we add it to \codeword{visited}. We check if our current position isn't by any chance the goal position. If that is the case we'll return the actions. 

If we are not at the goal positions, we'll ask the successors of our current node. The actions of our successors we add to the actions array. The position and action of our successors we add to our data structure. Then we start over by popping the data structure, etc.
 

\begin{python}

queue = util.DataStruct()

    # Push tuple with position and actions
    queue.push((problem.getStartState(), []))

    # Array of all visited nodes
    visited = []

    # Loop over the queue until it is empty
    while not queue.isEmpty():

        # Get the first node from the queue
        position, actions = queue.pop()

        # if this node is already visited go to the next node on the queue
        if position in visited:
            continue

        # Add node to the visited array
        visited.append(position)

        # If the node is the goal node, then return all actions performed to get to this node
        if problem.isGoalState(position):
            return actions

        # Get all neighbors of the current node and add them to the queue with the actions needed to get to this node
        neighbors = problem.getSuccessors(position)

        for nPos, nAction, nCost in neighbors:
            nActions = actions + [nAction]
            queue.push( (nPos, nActions) )
            

\end{python}


\section{Depth First Search Algorithm}

In the Depth First Search (DFS) we use a LIFO stack as data structure
this stack uses the principle of Last In First Out. This algorithm doesn't take long to calculate but is far from efficient. We can observe is the image below that Pacman will find the dot but not in the shortest way.

\begin{figure}[h]
  \includegraphics[height=125pt]{DFS.png}
    \centering
    \caption{DFS algorithm in tinyMaze }
    \label{fig:DFS}
\end{figure}


\section{Breadth First Search Algorithm}

Breadth First Search BFS is the same as DFS but using a FIFO queue in stead of a LIFO stack. The algorithm using the First In First Out principle has a longer calculation time, but is more efficient than DFS.

\begin{figure}[h]
  \includegraphics[height=125pt]{BFS.png}
    \centering
    \caption{BFS algorithm in tinyMaze }
    \label{fig:BFS}
\end{figure}


\section{Varying Uniform-Cost Search Algorithm}

The Varying Uniform-Cost Search algorithm (UCS) takes in consideration that not all the paths have the same cost. While the BFS algorithm assumes that every path has a cost of 1. It will just take the shortest path in terms of the numbers of actions. The way UCS works is using a Priority Queue as it's data structure.

\pagebreak

\section{A* Search Algorithm}

As seen in the previous section, the A star algorithm also makes use of a priority queue as it's data structure. The difference is that the A* Search Algorithm uses a heuristic function to decide the path it is going to take. The heuristic is a function that approaches the total path cost to the goal state. In question 4 we use the manhattanHeuristic. This function will take the shortest way to the dot (using 1 straight corner), without worrying about any walls in the playing field. A* will calculate the priority by taking the path cost together with the heuristic function. 

\begin{figure}[h]
	\includegraphics[height=125pt]{manhattanHeuristic.png}
	\centering
	\caption{Visualisation how manhattanHeuristic works}
	\label{ManhattanHeuristic}
\end{figure}

\chapter{Question 5 - 6: Corner Problem}

\section{Defining states}

In the previous problem a state was defined as a position, this however is not sufficient any more for our problem. Because our algorithms don't handle the same state twice we cannot use position as the sole thing to define our state. We can easily see why if pacman goes to a corner that is located in a corridor with a dead end. Now pacman can't go back out of the corridor and thus the algorithm has failed.

% Maybe add a figure here

A better way to define the state is the position where pacman is at plus which corners are already visited. This way pacman can visit the same position twice but not the same state.

Our goal state also had to be redefined because the goal could now be reached in 4 different locations at once. The goal state is reached if pacman has visited all four corners regardless of position.

\section{Creating a heuristic function}

\subsection{A simple heuristic}

Our first heuristic function was pretty simple and was both admissible and consistent. The heuristic function was the Manhattan distance from the position of pacman to the nearest corner of the map. This heuristic function gave us a good and working result but had too many node expansions to call it efficient.

We can visualise this with \textit{Figure \ref{fig:heuristic1}}.

\begin{figure}[h]
  \includegraphics[height=125pt]{heuristic1.png}
    \centering
    \caption{Visual representation of the heuristic function}
    \label{fig:heuristic1}
\end{figure}

\pagebreak

Below you can see how we implemented the simple function in python:

\begin{python}
def cornersHeuristic(state, problem: CornersProblem):
    corners = problem.corners
    walls = problem.walls

    if problem.isGoalState(state):
        return 0

    position, visitedCorners = state
    x, y = position
    costs = []

    for c in range(4):
        if not visitedCorners[c]:
            cor = corners[c]
            cost = util.manhattanDistance(position, cor)
            costs.append(cost)

    return min(costs)
\end{python}

\subsection{A better heuristic}

We already told that the previous heuristic function is not the most efficient one. This is because the function doesn't actually approach the goal but only the closest corner.

To better approach the goal we had to rethink the heuristic function. We took the previous heuristic function and expanded it with the most optimistic path to the goal.

A visualisation of how our heuristic function works can be seen in \textit{Figure \ref{fig:heuristic2}}.

\begin{figure}[h]
    \centering
    \includegraphics[height=125pt]{heuristic2.png}
    \caption{Visual representation of the heuristic function}
    \label{fig:heuristic2}
\end{figure}

To keep the calculations as fast as possible we pre calculate the optimistic paths for the corners. We saved these values in a LUT. 

\pagebreak

Here you can see the implementation of this function:

\begin{python}
# Begin CornerProblem class

# ...


long = 0
short = 0

if right > top:
    long = util.manhattanDistance( (right,top) , (1, top) )
    short = util.manhattanDistance( (right, top), (right, 1) )
else:
    short = util.manhattanDistance( (right,top) , (1, top) )
    long = util.manhattanDistance( (right, top), (right, 1) )

self.optimisticPaths = {
    0: 2 * short + long,
    1: long + short,
    2: short,
    3: 0,
    4: 0
}

# ...

# End CornersProblem class

def cornersHeuristic(state, problem: CornersProblem):
    corners = problem.corners
    walls = problem.walls

    if problem.isGoalState(state):
        return 0

    position, visitedCorners = state
    x, y = position
    costs = []

    if problem.isGoalState(state):
        return 0

    visitedCornersCount = sum(visitedCorners)

    for c in range(4):
        if not visitedCorners[c]:
            corner = corners[c]
            cost = util.manhattanDistance(position, corner)

            costs.append(cost)

    # problem.optimisticPaths is the LUT
    return min(costs) + problem.optimisticPaths[visitedCornersCount]
\end{python}

\chapter{Question 7 - 8: Food}

\section{Question 7: Food heuristic}

We created in total 3 heuristics with each iteration having a better node expansion efficiency.

\subsection{Heuristic 1: Manhattan Distance}

Our first heuristic is the Manhattan  distance to the closest dot. This was a consistent and admissible heuristic, but was not efficient enough. This was due to the fact that our heuristic didn't approach the path cost to the goal state enough.

\begin{figure}[h]
	\includegraphics[height=125pt]{Q7_Heuristic1.png}
	\centering
	\caption{Visualisation of Heuristic 1}
	\label{Q7Heuristic1}
\end{figure}

\subsection{Heuristic 2: Path cost to the goal state}

Our next iteration of our heuristic was the Manhatten distance to the closest dot plus the distance from that dot to the furthest dot away from this dot (see \ref{Q7Heuristic2}. This meant that the heuristic better approached the path cost to the goal state, and gave us better results.

\begin{figure}[h]
	\includegraphics[height=125pt]{Q7_Heuristic2.png}
	\centering
	\caption{Visualisation of Heuristic 2}
	\label{Q7Heuristic2}
\end{figure}


\subsection{Heuristic 3: Improving heuristic 2}

Heuristic 2 was a very efficient heuristic, but could be improved in node expansion efficiency. Instead of the Manhatten distance we now used the \codeword{mazeDistance} function. This meant that we drastically improved the node expansion efficiency, but had to sacrifice time efficiency.

The solve now took a total of 73 seconds on the tricky search, which was unacceptable. This ridiculous time was because the distances had to be recalculated every time a heuristic needed to be calculated. To counter this problem we cached the distances from all foods to the others before any heuristic needed to be calculated. This already improved our time of 73s to a measly 1s. However some distances weren't being cached. So now when the heuristic encountered an uncached distance it calculated the distance and cached it. This improved the timing to 0.1-0.7s.

\section{Question 8: Closest dot}

In the previous problem if we used smaller mazes our algorithm could quickly find a solution for the problem, however when the amount of food or the size of the maze increased it became harder and harder for our algorithm to solve these mazes.

The reason was because the amount of nodes increased exponentially when the amount of food increased. Question 8 proposed a solution but a sub optimal one to this problem. Instead of finding the lowest cost path to the goal state, find the lowest cost path to the closest dot.

The implementation was quite simple with just 2 lines of code. One to say that the goal is reached in the "any food problem" when pacman reaches a state where he is on top of a food item. The other to solve the problem with an optimal search algorithm (like breadth first search, Uniform Cost Search or A*). We chose breadth first search.
\end{document}
